class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players = {})
    @guesser = players[:guesser]
    @referee = players[:referee]
    @board = nil
  end

  def setup
    word_length = @referee.pick_secret_word
    @guesser.register_secret_length(word_length)
    @board = Array.new(word_length)
  end

  def take_turn
    puts "secret word: ?????#{board.length}"
    char = @guesser.guess(@board)
    indexes_of_char = @referee.check_guess(char)
    @guesser.handle_response(char, indexes_of_char)
    update_board(indexes_of_char, char)
  end

  def update_board(inds, c)
    inds.each { |i| @board[i] = c }
  end
end

class HumanPlayer
  def initialize

  end

  def pick_secret_word
    puts 'choose a word'
    gets.chomp.length
  end

  def check_guess(letter)
    puts "The other player guesses: #{letter}"
    puts 'What indexes contain this letter?'
    usr = gets.chomp
    usr.split(',').map(&:to_i)
  end
end

class ComputerPlayer

  def initialize(dictionary)
    @dictionary = dictionary
    @secret_word = nil
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def check_guess(char)
    arr = []
    @secret_word.chars.each_with_index { |c, i| arr << i if c == char }
    arr
  end

  def register_secret_length(len)
    @secret_length = len
    @dictionary.select! do |w|
      w.length == @secret_length
    end
  end

  def guess(board)
    letter_freq = Hash.new(0)
    @dictionary.each do |w|
      w.chars.each do |l|
        letter_freq[l] += 1
      end
    end
    ('abcdefghijklmnopqrstuvwxyz'.chars - board).reduce do |acc, chr|
      letter_freq[chr] > letter_freq[acc] ? chr : acc
    end
  end

  def handle_response(letter, inds)
    if inds.empty?
      @dictionary.reject! do |w|
        w =~ /#{letter}/
      end
    else
      partial = Array.new(@secret_length)
      inds.each { |i| partial[i] = letter }
      reg = partial.map { |c| c.nil? ? "[^#{letter}]" : c }.join
      @dictionary.select! do |w|
        w =~ /^#{reg}$/
      end
    end
  end

  def candidate_words
    @dictionary
  end
end
#File.open(File.dirname(__FILE__) + '/dictionary.txt', 'r').readlines.select { |w| w.chomp.length == @secret_length }
